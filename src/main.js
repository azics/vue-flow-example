import { createApp } from 'vue'
import App from './App.vue'
import './main.css'; // Import your CSS file here


createApp(App).mount('#app')
